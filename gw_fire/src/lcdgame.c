// Unique per LCD game
#include <pebble.h>

#include "gamecore.h"

static GBitmap *s_lcdseg_bmps[LCDSEG_COUNT] = {
  NULL,
};

typedef struct {
  uint32_t rsrc_id;
  int rot;
  int x; // Center of the segment icon
  int y; // Bottom of the segment icon
} LcdGameLayout;

// Res:
//Non-Rotate:0x90, 0xA8
//    Rotate:0xA8, 0x90

static const LcdGameLayout s_lcdseg_lyt[LCDSEG_COUNT] = {
#define PLAYERSEG_X_BASE 32
#define PLAYERSEG_X_STEP 42
#define PLAYERSEG_Y      136
#define MISSSEG_Y        144

#define FAILSEG_X_BASE 158
#define FAILSEG_X_STEP -14
#define FAILSEG_Y      12

#define FIRESEG_X 38
#define FIRESEG_Y 24

#define AMBULSEG_X 152
#define AMBULSEG_Y 144

#define BUILDSEG_X 9
#define BUILDSEG_Y 144

#define BOUNCE0_X (PLAYERSEG_X_BASE)
#define BOUNCE1_X (BOUNCE0_X + PLAYERSEG_X_STEP)
#define BOUNCE2_X (BOUNCE1_X + PLAYERSEG_X_STEP)

#define CHARROW_Y_BASE 126
#define CHARROW_Y_STEP 20
#define CHARROW0_Y  (CHARROW_Y_BASE - (0 * CHARROW_Y_STEP))
#define CHARROW1_Y  (CHARROW_Y_BASE - (1 * CHARROW_Y_STEP))
#define CHARROW2_Y  (CHARROW_Y_BASE - (2 * CHARROW_Y_STEP))
#define CHARROW2_5_Y (CHARROW_Y_BASE - (2.5 * CHARROW_Y_STEP))
#define CHARROW3_Y  (CHARROW_Y_BASE - (3 * CHARROW_Y_STEP))
#define CHARROW4_Y  (CHARROW_Y_BASE - (3.75 * CHARROW_Y_STEP))
  { RESOURCE_ID_PLAYER_SEG0, 0, // LCDSEG_PLAYER__POSITION_0
    PLAYERSEG_X_BASE + (0 * PLAYERSEG_X_STEP),
    PLAYERSEG_Y,
    },
  { RESOURCE_ID_PLAYER_SEG1, 0, // LCDSEG_PLAYER__POSITION_1
    PLAYERSEG_X_BASE + (1 * PLAYERSEG_X_STEP),
    PLAYERSEG_Y,
    },
  { RESOURCE_ID_PLAYER_SEG2, 0, // LCDSEG_PLAYER__POSITION_2
    PLAYERSEG_X_BASE + (2 * PLAYERSEG_X_STEP),
    PLAYERSEG_Y,
    },

  { RESOURCE_ID_MISS_SEG0, 0, // LCDSEG_MISS__POSITION_0
    PLAYERSEG_X_BASE + (0 * PLAYERSEG_X_STEP),
    MISSSEG_Y,
    },
  { RESOURCE_ID_MISS_SEG1, 0, // LCDSEG_MISS__POSITION_1
    PLAYERSEG_X_BASE + (1 * PLAYERSEG_X_STEP),
    MISSSEG_Y,
    },
  { RESOURCE_ID_MISS_SEG2, 0, // LCDSEG_MISS__POSITION_2
    PLAYERSEG_X_BASE + (2 * PLAYERSEG_X_STEP),
    MISSSEG_Y,
    },

  { RESOURCE_ID_FAIL_SEG, 0, // LCDSEG_FAIL__POSITION_0
    FAILSEG_X_BASE + (0 * FAILSEG_X_STEP),
    FAILSEG_Y,
    },
  { RESOURCE_ID_FAIL_SEG, 0, // LCDSEG_FAIL__POSITION_1
    FAILSEG_X_BASE + (1 * FAILSEG_X_STEP),
    FAILSEG_Y,
    },
  { RESOURCE_ID_FAIL_SEG, 0, // LCDSEG_FAIL__POSITION_2
    FAILSEG_X_BASE + (2 * FAILSEG_X_STEP),
    FAILSEG_Y,
    },

  { RESOURCE_ID_FIRE_SEG0, 0, // LCDSEG_FIRE__POSITION_0
    FIRESEG_X,
    FIRESEG_Y,
    },
  { RESOURCE_ID_FIRE_SEG1, 0, // LCDSEG_FIRE__POSITION_1
    FIRESEG_X,
    FIRESEG_Y,
    },
  { RESOURCE_ID_FIRE_SEG2, 0, // LCDSEG_FIRE__POSITION_2
    FIRESEG_X,
    FIRESEG_Y,
    },
  { RESOURCE_ID_FIRE_SEG3, 0, // LCDSEG_FIRE__POSITION_3
    FIRESEG_X,
    FIRESEG_Y,
    },

  { RESOURCE_ID_CHAR_SEG8,  0, // LCDSEG_CHAR__ARC_0__POSITION_0
    14,
    CHARROW4_Y - 8,
    },
  { RESOURCE_ID_CHAR_SEG9,  0, // LCDSEG_CHAR__ARC_0__POSITION_1
    12,
    CHARROW2_5_Y - 4,
    },
  { RESOURCE_ID_CHAR_SEG4,  0, // LCDSEG_CHAR__ARC_0__POSITION_2 // ?
    22,
    CHARROW3_Y,
    },
  { RESOURCE_ID_CHAR_SEG7,  0, // LCDSEG_CHAR__ARC_0__POSITION_3 // ?
    26,
    CHARROW2_Y,
    },
  { RESOURCE_ID_CHAR_SEG6,  0, // LCDSEG_CHAR__ARC_0__POSITION_4 // ?
    30,
    CHARROW1_Y,
    },
  { RESOURCE_ID_CHAR_SEG0,  0, // LCDSEG_CHAR__ARC_0__POSITION_5
    BOUNCE0_X,
    CHARROW0_Y,
    },

  { RESOURCE_ID_CHAR_SEG1,  0, // LCDSEG_CHAR__ARC_1__POSITION_0
    BOUNCE0_X + 6,
    CHARROW1_Y,
    },
  { RESOURCE_ID_CHAR_SEG4,  1, // LCDSEG_CHAR__ARC_1__POSITION_1
    BOUNCE0_X + 12,
    CHARROW2_Y,
    },
  { RESOURCE_ID_CHAR_SEG3,  2, // LCDSEG_CHAR__ARC_1__POSITION_2
    BOUNCE0_X + 18,
    CHARROW3_Y,
    },
  { RESOURCE_ID_CHAR_SEG5,  2, // LCDSEG_CHAR__ARC_1__POSITION_3
    BOUNCE0_X + 28,
    CHARROW4_Y,
    },
  { RESOURCE_ID_CHAR_SEG5,  3, // LCDSEG_CHAR__ARC_1__POSITION_4
    BOUNCE0_X + 38,
    CHARROW3_Y,
    },
  { RESOURCE_ID_CHAR_SEG6,  3, // LCDSEG_CHAR__ARC_1__POSITION_5
    BOUNCE0_X + 40,
    CHARROW2_Y,
    },
  { RESOURCE_ID_CHAR_SEG5,  0, // LCDSEG_CHAR__ARC_1__POSITION_6
    BOUNCE0_X + 42,
    CHARROW1_Y,
    },
  { RESOURCE_ID_CHAR_SEG5,  0, // LCDSEG_CHAR__ARC_1__POSITION_7
    BOUNCE1_X,
    CHARROW0_Y,
    },

  { RESOURCE_ID_CHAR_SEG4,  1, // LCDSEG_CHAR__ARC_2__POSITION_0
    BOUNCE1_X + 8,
    CHARROW1_Y,
    },
  { RESOURCE_ID_CHAR_SEG2,  1, // LCDSEG_CHAR__ARC_2__POSITION_1
    BOUNCE1_X + 12,
    CHARROW2_Y,
    },
  { RESOURCE_ID_CHAR_SEG0,  2, // LCDSEG_CHAR__ARC_2__POSITION_2
    BOUNCE1_X + 22,
    CHARROW3_Y,
    },
  { RESOURCE_ID_CHAR_SEG7,  3, // LCDSEG_CHAR__ARC_2__POSITION_3
    BOUNCE1_X + 32,
    CHARROW2_Y,
    },
  { RESOURCE_ID_CHAR_SEG4,  0, // LCDSEG_CHAR__ARC_2__POSITION_4 // ?
    BOUNCE1_X + 38,
    CHARROW1_Y,
    },
  { RESOURCE_ID_CHAR_SEG7,  0, // LCDSEG_CHAR__ARC_2__POSITION_5
    BOUNCE2_X,
    CHARROW0_Y,
    },

  { RESOURCE_ID_CHAR_SEG4,  0, // LCDSEG_CHAR__ARC_3__POSITION_0 // ?
    BOUNCE2_X + 6,
    CHARROW1_Y,
    },
  { RESOURCE_ID_CHAR_SEG5,  0, // LCDSEG_CHAR__ARC_3__POSITION_1 // ?
    BOUNCE2_X + 18,
    CHARROW2_Y,
    },
  { RESOURCE_ID_CHAR_SEG5,  0, // LCDSEG_CHAR__ARC_3__POSITION_2 // ?
    BOUNCE2_X + 30,
    CHARROW1_Y,
    },

  { RESOURCE_ID_AMBUL_SEG,  0, // LCDSEG_AMBUL
    AMBULSEG_X,
    AMBULSEG_Y,
    },
  { RESOURCE_ID_BUILD_SEG,  0, // LCDSEG_BUILDING,
    BUILDSEG_X,
    BUILDSEG_Y,
    },
};

const GPoint g_score_pos = {
  .x = 112,
  .y = 4,
};

void lcdgameimpl_player_bumped(GameCoreState *wrk, bool direction_up) {
  // Fire doesn't use this
}
void lcdgameimpl_player_select(GameCoreState *wrk) {
  // Fire doesn't use this
}

void lcdgameimpl_init(GameCoreState *wrk) {
}

static int prv_impl_rank(GameCoreState *wrk) {
  int score = wrk->player.score;
  if(wrk->game_b) {
    score += 100;
    score *= 2;
  }
  return score;
}

uint32_t lcdgameimpl_speed(GameCoreState *wrk) {
  static const int s_speeds[40] = {
    800, 750, 700, 675, 650,
    625, 600, 580, 560, 540,
    520, 500, 485, 470, 455,
    440, 425, 415, 405, 395,

    385, 375, 370, 365, 360,
    355, 350, 345, 340, 335,
    330, 325, 320, 315, 310,
    305, 300, 295, 290, 285,
  };
  int score = prv_impl_rank(wrk);
  int idx = score / 10;
  if(idx >= 40) {
    idx = s_speeds[39] - ((score - 400) / 3);
    if(idx < 10)
      idx = 10;
    return idx;
  }
  return s_speeds[idx];
}

int lcdgameimpl_spawn_rate(GameCoreState *wrk) {
  static const int s_spawn_rates[20] = {
    20, 19, 18, 17, 16,
    15, 14, 14, 13, 13,
    12, 12, 12, 11, 11,
    11, 11, 10, 10, 10,
  };
  int score = prv_impl_rank(wrk);
  int idx = score / 10;
  if(idx >= 20)
    idx = 19;
  return s_spawn_rates[idx];
}

int lcdgameimpl_spawn_variance(GameCoreState *wrk) {
  int score = prv_impl_rank(wrk);
  int rate = (score / 40);
  if(rate > 10)
    return 10;
  return rate;
}

static void prv_impl_update_segments(GameCoreState *wrk) {
  uint32_t state = wrk->lcdgame.impl.obj_state;
  for(int i = LCDSEG_CHAR__BASE; i <= LCDSEG_CHAR__LAST; i++) {
    lcdgame_segment_set(wrk, i, state & 1);
    state >>= 1;
  }
  lcdgame_segment_set(wrk, LCDSEG_MISS__POSITION_0, wrk->lcdgame.trigger_flags & (1 << 0));
  lcdgame_segment_set(wrk, LCDSEG_MISS__POSITION_1, wrk->lcdgame.trigger_flags & (1 << 1));
  lcdgame_segment_set(wrk, LCDSEG_MISS__POSITION_2, wrk->lcdgame.trigger_flags & (1 << 2));
  lcdgame_segment_set(wrk, LCDSEG_FAIL__POSITION_0, wrk->player.misses > 0);
  lcdgame_segment_set(wrk, LCDSEG_FAIL__POSITION_1, wrk->player.misses > 1);
  lcdgame_segment_set(wrk, LCDSEG_FAIL__POSITION_2, wrk->player.misses > 2);
  lcdgame_segment_set(wrk, LCDSEG_FIRE__POSITION_0, ((wrk->lcdgame.counter + 3) % 10) >= 4);
  lcdgame_segment_set(wrk, LCDSEG_FIRE__POSITION_1, ((wrk->lcdgame.counter + 2) % 10) >= 4);
  lcdgame_segment_set(wrk, LCDSEG_FIRE__POSITION_2, ((wrk->lcdgame.counter + 1) % 10) >= 4);
  lcdgame_segment_set(wrk, LCDSEG_FIRE__POSITION_3, ((wrk->lcdgame.counter + 0) % 10) >= 4);
  lcdgame_segment_set(wrk, LCDSEG_AMBUL, 1);
  if((wrk->lcdgame.lcdseg_status[LCDSEG_CHAR__ARC_0__POSITION_0] & 2) ||
     (wrk->lcdgame.lcdseg_status[LCDSEG_CHAR__ARC_0__POSITION_1] & 2))
    lcdgame_segment_set(wrk, LCDSEG_BUILDING, 0);
  lcdgame_segment_set(wrk, LCDSEG_BUILDING, 1);
}

static void prv_impl_load_timer(GameCoreState *wrk) {
  int tmr, var;
  tmr = lcdgameimpl_spawn_rate(wrk);
  var = lcdgameimpl_spawn_variance(wrk);
  if(var) {
    tmr += (rand() % (var*2 + 1)) - var;
  }
  if(tmr < 2)
    tmr = 2;
  wrk->lcdgame.impl.timer = tmr;
}
static void prv_impl_add_object(GameCoreState *wrk) {
  wrk->lcdgame.impl.timer--;
  if(wrk->lcdgame.impl.timer == 0 || wrk->lcdgame.impl.obj_state == 0) {
    wrk->lcdgame.impl.obj_state |= 1 << (rand() & 1);
    prv_impl_load_timer(wrk);
  }
}

void lcdgameimpl_triggered(GameCoreState *wrk) {
  lcdgame_add_score(wrk, 1);
}

void lcdgameimpl_update(GameCoreState *wrk) {
  if(wrk->lcdgame.counter == 0) { // Initialize game
    for(int i = 0; i < LCDSEG_COUNT; i++) {
      lcdgame_segment_set(wrk, i, 0);
    }
    wrk->lcdgame.trigger_flags = 0;
    wrk->lcdgame.impl.obj_state = 0;
    wrk->lcdgame.impl.timer = 1;
    lcdgame_player_moved(wrk);
  }
  if(wrk->lcdgame.impl.obj_state & 3) {
    int val = wrk->lcdgame.impl.obj_state & 3;
    wrk->lcdgame.impl.obj_state &= ~3;
    wrk->lcdgame.impl.obj_state |= val << 1;
  }
  wrk->lcdgame.impl.obj_state <<= 1;
  wrk->lcdgame.impl.obj_state &= (1 << (6+8+6+3)) - 1;

  prv_impl_add_object(wrk);
  prv_impl_update_segments(wrk);

  if(wrk->lcdgame.impl.obj_state & (1 << (6+0+0-1))) {
    wrk->lcdgame.trigger_flags |= (1 << 0);
  }
  if(wrk->lcdgame.impl.obj_state & (1 << (6+8+0-1))) {
    wrk->lcdgame.trigger_flags |= (1 << 1);
  }
  if(wrk->lcdgame.impl.obj_state & (1 << (6+8+6-1))) {
    wrk->lcdgame.trigger_flags |= (1 << 2);
  }
}

void lcdgameimpl_player_died(GameCoreState *wrk) {
  // TODO: Find the object that killed player, and update its position
  if(wrk->lcdgame.trigger_flags & (1 << 0)) {
    wrk->lcdgame.impl.obj_state &= ~(1 << (6+0+0-1));
  }
  if(wrk->lcdgame.trigger_flags & (1 << 1)) {
    wrk->lcdgame.impl.obj_state &= ~(1 << (6+8+0-1));
  }
  if(wrk->lcdgame.trigger_flags & (1 << 2)) {
    wrk->lcdgame.impl.obj_state &= ~(1 << (6+8+6-1));
  }
  prv_impl_update_segments(wrk);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// LCDGame commonality
static void prv_mark_dirty(GameCoreState *wrk) {
  layer_mark_dirty(window_get_root_layer(wrk->window));
}

static GColor s_draw_pal[2][2];

static void prv_draw_segment(GameCoreState *wrk, int segment, int on, GContext *ctx) {
  GRect rect;
  rect = gbitmap_get_bounds(s_lcdseg_bmps[segment]);
  gbitmap_set_palette(s_lcdseg_bmps[segment], s_draw_pal[on], false);
  graphics_context_set_compositing_mode(ctx, GCompOpSet);

  GPoint dest_ic, src_ic;
  src_ic.x = rect.size.w / 2;
  src_ic.y = rect.size.h / 2;

  int rot = s_lcdseg_lyt[segment].rot;
  if(ROTATED_SCREEN) {
    dest_ic.x = s_lcdseg_lyt[segment].y - src_ic.y;
    dest_ic.y = s_lcdseg_lyt[segment].x;
    rot += 3;
  }else{
    dest_ic.x = s_lcdseg_lyt[segment].x;
    dest_ic.y = s_lcdseg_lyt[segment].y - src_ic.y;
  }
  if(wrk->rot180) {
    rot += 2;
    dest_ic.x = 0x90 - dest_ic.x - 1;
  }else{
    dest_ic.y = 0xA8 - dest_ic.y - 1;
  }
  rot &= 3;
  rot = TRIG_MAX_ANGLE * rot / 4;
  graphics_draw_rotated_bitmap(ctx, s_lcdseg_bmps[segment], src_ic, rot, dest_ic);
}

void lcdgame_segment_on(GameCoreState *wrk, int segment) {
  if((wrk->lcdgame.lcdseg_status[segment] & 1) == 1)
    return;
  wrk->lcdgame.lcdseg_status[segment] &= ~3;
  wrk->lcdgame.lcdseg_status[segment] |= 1 | 2;
  prv_mark_dirty(wrk);
}

void lcdgame_segment_off(GameCoreState *wrk, int segment) {
  if((wrk->lcdgame.lcdseg_status[segment] & 1) == 0)
    return;
  wrk->lcdgame.lcdseg_status[segment] &= ~3;
  wrk->lcdgame.lcdseg_status[segment] |= 0 | 2;
  prv_mark_dirty(wrk);
}

void lcdgame_segment_set(GameCoreState *wrk, int segment, int on) {
  if(on) {
    lcdgame_segment_on(wrk, segment);
  }else{
    lcdgame_segment_off(wrk, segment);
  }
}

static void prv_update_speed(GameCoreState *wrk) {
  wrk->speed = lcdgameimpl_speed(wrk);
}

void lcdgame_add_score(GameCoreState *wrk, int count) {
  player_add_score(wrk, count);
}

static void prv_load_segments(GameCoreState *wrk) {
  for(int i = 0; i < LCDSEG_COUNT; i++) {
    GBitmap *tmp_bmp = gbitmap_create_with_resource(s_lcdseg_lyt[i].rsrc_id);
    s_lcdseg_bmps[i] = gbitmap_create_palettized_from_1bit(tmp_bmp);
    gbitmap_destroy(tmp_bmp);
  }
}
static void prv_unload_segments(GameCoreState *wrk) {
  for(int i = 0; i < LCDSEG_COUNT; i++) {
    gbitmap_destroy(s_lcdseg_bmps[i]);
  }
}

// 7seg emulation
static const uint8_t s_digit_points[10] = {
/*
 _  0
|_|123
|_|456
*/
  0b01111011, // 0
  0b01001000, // 1
  0b00111101, // 2
  0b01101101, // 3
  0b01001110, // 4
  0b01100111, // 5
  0b01110111, // 6
  0b01001001, // 7
  0b01111111, // 8
  0b01101111, // 9
};
static const int s_7seg_lines[7][2][2] = {
  { { 0, 0 }, { 1, 0, } },
  { { 0, 0 }, { 0, 1, } },
  { { 0, 1 }, { 1, 1, } },
  { { 1, 0 }, { 1, 1, } },
  { { 0, 1 }, { 0, 2, } },
  { { 0, 2 }, { 1, 2, } },
  { { 1, 1 }, { 1, 2, } },
};

static GPoint prv_get_point(GPoint pos) {
  GPoint p;
  if(ROTATED_SCREEN) {
    p.x = pos.y;
    p.y = 0xA8 - pos.x - 1;
  }else{
    p.x = pos.x;
    p.y = pos.y;
  }
  return p;
}

static void prv_draw_seg(GContext *ctx, int seg, GPoint pos, int size) {
  GPoint a, b;
  int ax,ay, bx,by;
  ax = s_7seg_lines[seg][0][0];
  ay = s_7seg_lines[seg][0][1];
  bx = s_7seg_lines[seg][1][0];
  by = s_7seg_lines[seg][1][1];

  a.x = pos.x + ax*size;
  a.y = pos.y + ay*size;
  b.x = pos.x + bx*size;
  b.y = pos.y + by*size;

  graphics_draw_line(ctx, prv_get_point(a), prv_get_point(b));
}
static void prv_draw_digit(GContext *ctx, int val, GPoint pos) {
  for(int i = 0; i < 7; i++) {
    if(s_digit_points[val] & (1 << i))
      prv_draw_seg(ctx, i, pos, 6);
  }
}

static void prv_draw_score(GameCoreState *wrk, GContext *ctx, GPoint pos) {
  graphics_context_set_stroke_color(ctx, GColorBlack);
  graphics_context_set_antialiased(ctx, false);
  graphics_context_set_stroke_width(ctx, 3);
  int score = wrk->player.score;
  for(int i = 0; i < 3; i++) {
    if(i == 0 || score)
      prv_draw_digit(ctx, score % 10, pos);
    pos.x -= 10;
    score /= 10;
  }
}

static void prv_erase_score(GContext *ctx, GPoint pos) {
  GRect score_rect;
  GPoint tl, br;
  GPoint raw_tl, raw_br;
#define ERASE_BOX_FUDGE 2
  raw_tl = pos;
  raw_br = raw_tl;
  raw_br.x += 6*1;
  raw_br.y += 6*2;
  raw_tl.x -= ERASE_BOX_FUDGE;
  raw_tl.y -= ERASE_BOX_FUDGE;
  raw_br.x += ERASE_BOX_FUDGE;
  raw_br.y += ERASE_BOX_FUDGE;
  tl = prv_get_point(raw_tl);
  br = prv_get_point(raw_br);
  if(tl.x < br.x) {
    score_rect.origin.x = tl.x;
    score_rect.size.w = br.x - tl.x;
  }else{
    score_rect.origin.x = br.x;
    score_rect.size.w = tl.x - br.x;
  }
  if(tl.y < br.y) {
    score_rect.origin.y = tl.y;
    score_rect.size.h = br.y - tl.y;
  }else{
    score_rect.origin.y = br.y;
    score_rect.size.h = tl.y - br.y;
  }
  // Clear the score area
  graphics_context_set_stroke_color(ctx, GColorWhite);
  graphics_context_set_fill_color(ctx, GColorWhite);
  graphics_fill_rect(ctx, score_rect, 0, 0);
}

static void prv_update_proc(Layer *layer, GContext *ctx) {
  GameCoreState *wrk = window_get_user_data(layer_get_window(layer));
  for(int i = 0; i < LCDSEG_COUNT; i++) {
    if(wrk->lcdgame.lcdseg_status[i] & 2) {
      prv_draw_segment(wrk, i, wrk->lcdgame.lcdseg_status[i] & 1, ctx);
      wrk->lcdgame.lcdseg_status[i] &= ~2;
    }
  }
  GPoint pos = g_score_pos;
  for(int i = 0; i < 3; i++) {
    prv_erase_score(ctx, pos);
    pos.x -= 10;
  }
  prv_draw_score(wrk, ctx, g_score_pos);
}

void lcdgame_init(GameCoreState *wrk) {
  s_draw_pal[0][0] = GColorWhite;
  s_draw_pal[0][1] = GColorClear;
  s_draw_pal[1][0] = GColorBlack;
  s_draw_pal[1][1] = GColorClear;

  wrk->lcdgame.trigger_flags = 0;
  wrk->lcdgame.trigger_compare = 0;
  prv_load_segments(wrk);
  layer_set_update_proc(window_get_root_layer(wrk->window), prv_update_proc);
  for(int i = 0; i < LCDSEG_COUNT; i++) {
    wrk->lcdgame.lcdseg_status[i] = 1 | 2;
  }
  prv_update_speed(wrk);
}
void lcdgame_finish(GameCoreState *wrk) {
  prv_unload_segments(wrk);
}

void lcdgame_update(GameCoreState *wrk) {
  if(wrk->lcdgame.trigger_flags != wrk->lcdgame.trigger_compare) {
    if(!player_kill(wrk)) {
      wrk->game_over = true;
    }
    lcdgameimpl_player_died(wrk);
    wrk->speed = DEATH_SPEED_DELAY;
    wrk->lcdgame.trigger_flags = 0;
  }else{
    lcdgameimpl_update(wrk);
    if(wrk->lcdgame.trigger_flags & (1 << wrk->player.position)) {
      lcdgameimpl_triggered(wrk);
      wrk->lcdgame.trigger_flags &= ~(1 << wrk->player.position);
    }
    prv_update_speed(wrk);
  }
  wrk->lcdgame.counter++;
}

void lcdgame_player_moved(GameCoreState *wrk) {
  rand();
  if(wrk->lcdgame.trigger_flags & (1 << wrk->player.position)) {
    lcdgameimpl_triggered(wrk);
    wrk->lcdgame.trigger_flags &= ~(1 << wrk->player.position);
  }
  for(int i = 0; i < PLAYER_POSITION_COUNT; i++) {
    lcdgame_segment_off(wrk, LCDSEG_PLAYER__BASE+i);
  }
  lcdgame_segment_on(wrk, LCDSEG_PLAYER__BASE+wrk->player.position);
}
void lcdgame_player_bumped(GameCoreState *wrk, bool direction_up) {
  rand();
  lcdgameimpl_player_bumped(wrk, direction_up);
}
void lcdgame_player_select(GameCoreState *wrk) {
  rand();
  lcdgameimpl_player_select(wrk);
}
