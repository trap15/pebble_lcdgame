#pragma once

#include "player.h"
#include "lcdgame.h"

typedef struct GameCoreState {
  Window *window;
  AppTimer *update_tmr;

  // Config
  bool move_invert;
  bool rot180;
  bool game_b;

  // Game state
  uint32_t speed;
  PlayerState player;
  LcdGameState lcdgame;
  bool game_over;
} GameCoreState;

bool game_core_init(GameCoreState *wrk);
void game_core_finish(GameCoreState *wrk);
