#include <pebble.h>

#include "gamecore.h"

GameCoreState g_game;

int main(void) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Initializing '" GAME_NAME "'");
  if(!game_core_init(&g_game))
    return 0;
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Initialized.");
  app_event_loop();
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Exiting.");
  game_core_finish(&g_game);
}
