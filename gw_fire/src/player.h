#pragma once

typedef struct GameCoreState GameCoreState;

#define PLAYER_POSITION_MAX (PLAYER_POSITION_COUNT-1)

typedef struct PlayerState {
  uint8_t misses;
  uint8_t position; // [0,PLAYER_POSITION_MAX]
  uint16_t score;
} PlayerState;

void player_init(GameCoreState *wrk);
void player_finish(GameCoreState *wrk);

// Return false if game over
bool player_kill(GameCoreState *wrk);

void player_inp_up(GameCoreState *wrk);
void player_inp_down(GameCoreState *wrk);
void player_inp_select(GameCoreState *wrk);

void player_add_score(GameCoreState *wrk, int value);
