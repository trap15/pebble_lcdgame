#include <pebble.h>

#include "gamecore.h"

static void prv_timer_callback(void *data);

static void prv_timer_unregister(GameCoreState *wrk) {
  if(wrk->update_tmr != NULL) {
    app_timer_cancel(wrk->update_tmr);
    wrk->update_tmr = NULL;
  }
}
static void prv_timer_reload(GameCoreState *wrk) {
  prv_timer_unregister(wrk);
  wrk->update_tmr = app_timer_register(wrk->speed, prv_timer_callback, wrk);
}

static void prv_timer_callback(void *data) {
  GameCoreState *wrk = (GameCoreState*)data;
  lcdgame_update(wrk);
  if(!wrk->game_over)
    prv_timer_reload(wrk);
}

static void prv_select_click_handler(ClickRecognizerRef recognizer, void *context) {
  GameCoreState *wrk = (GameCoreState*)context;
  player_inp_select(wrk);
}

static void prv_up_click_handler(ClickRecognizerRef recognizer, void *context) {
  GameCoreState *wrk = (GameCoreState*)context;
  if(!wrk->move_invert) {
    player_inp_up(wrk);
  }else{
    player_inp_down(wrk);
  }
}

static void prv_down_click_handler(ClickRecognizerRef recognizer, void *context) {
  GameCoreState *wrk = (GameCoreState*)context;
  if(!wrk->move_invert) {
    player_inp_down(wrk);
  }else{
    player_inp_up(wrk);
  }
}

static void prv_click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, prv_select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, prv_up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, prv_down_click_handler);
}

static void prv_window_load(Window *window) {
}

static void prv_window_unload(Window *window) {
}

bool game_core_init(GameCoreState *wrk) {
  wrk->speed = UINT32_MAX;
  wrk->update_tmr = NULL;
  wrk->game_over = false;

  wrk->move_invert = false;
  wrk->rot180 = false;
  wrk->game_b = true;

  wrk->window = window_create();
  window_set_click_config_provider_with_context(wrk->window, prv_click_config_provider, wrk);
  window_set_window_handlers(wrk->window, (WindowHandlers) {
    .load = prv_window_load,
    .unload = prv_window_unload,
  });
  window_set_user_data(wrk->window, wrk);
  window_stack_push(wrk->window, false);

  lcdgame_init(wrk);
  player_init(wrk);
  // Start it up!
  prv_timer_reload(wrk);
  return true;
}

void game_core_finish(GameCoreState *wrk) {
  prv_timer_unregister(wrk);
  player_finish(wrk);
  lcdgame_finish(wrk);
}
