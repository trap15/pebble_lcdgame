#include <pebble.h>

#include "gamecore.h"

void player_init(GameCoreState *wrk) {
  wrk->player.misses = 0;
  wrk->player.position = 0;
  wrk->player.score = 0;
}
void player_finish(GameCoreState *wrk) {
}

bool player_kill(GameCoreState *wrk) {
  if(wrk->player.misses < PLAYER_LIVES) {
    wrk->player.misses++;
  }
  return wrk->player.misses < PLAYER_LIVES;
}

void player_inp_up(GameCoreState *wrk) {
  if(wrk->player.position < PLAYER_POSITION_MAX) {
    wrk->player.position++;
    lcdgame_player_moved(wrk);
  }else{
    lcdgame_player_bumped(wrk, true);
  }
}
void player_inp_down(GameCoreState *wrk) {
  if(wrk->player.position > 0) {
    wrk->player.position--;
    lcdgame_player_moved(wrk);
  }else{
    lcdgame_player_bumped(wrk, false);
  }
}
void player_inp_select(GameCoreState *wrk) {
  lcdgame_player_select(wrk);
}

void player_add_score(GameCoreState *wrk, int value) {
  wrk->player.score += value;
}
