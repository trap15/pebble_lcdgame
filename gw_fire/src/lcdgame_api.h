// API that all LCD games implement
#pragma once

typedef struct GameCoreState GameCoreState;

// Basic initialization
void lcdgame_init(GameCoreState *wrk);
// Cleanup
void lcdgame_finish(GameCoreState *wrk);

// Timer update (may change timer speed)
void lcdgame_update(GameCoreState *wrk);
// Player movement update
void lcdgame_player_moved(GameCoreState *wrk);
// Player failed movement update
void lcdgame_player_bumped(GameCoreState *wrk, bool direction_up);
// Player select button update
void lcdgame_player_select(GameCoreState *wrk);

void lcdgame_add_score(GameCoreState *wrk, int count);

void lcdgame_segment_on(GameCoreState *wrk, int segment);
void lcdgame_segment_off(GameCoreState *wrk, int segment);
void lcdgame_segment_set(GameCoreState *wrk, int segment, int on);
